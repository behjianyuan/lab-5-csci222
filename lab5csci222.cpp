///Lab 5 CSCI222
#include <iostream>
using namespace std;


int main()
{
    double celcius, fahrenheit;
    cout << "Enter temperature in Celcius: ";
    cin >> celcius;
    fahrenheit = (celcius * 9.0) / 5.0 + 32;
    cout << "\nCONVERTED!!" << endl;
    cout << "Celcius: " << celcius << endl;
    cout << "Fahrenheit: " << fahrenheit << endl;
    return 0;
}

